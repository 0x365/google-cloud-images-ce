FROM gcr.io/google.com/cloudsdktool/cloud-sdk:324.0.0-alpine
WORKDIR /app

ENV PACKER_VERSION=1.6.6 \
    BUILD=true \
    REGION=us-central1 \
    ZONE=us-central1-a \
    ENVIRONMENT=test \
    USER=build \
    DISK_SIZE=20 \
    PACKER_FILE_PATH=/app/test/image.json \
    PLAYBOOK_FILE_PATH=/app/test/playbook.yml

RUN set -e \
    && apk add --no-cache ansible unzip wget gettext \
    && rm -f /var/cache/apk/*

RUN wget https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip \
    && unzip packer_${PACKER_VERSION}_linux_amd64.zip \
    && rm packer_${PACKER_VERSION}_linux_amd64.zip \
    && mv packer /usr/bin

COPY test /app/test
COPY pipe /usr/bin/
RUN chmod +x /usr/bin/pipe.sh
RUN chmod +x /usr/bin/replace_tokens.sh

ENTRYPOINT ["/usr/bin/pipe.sh"]