#!/usr/bin/env sh

# Create an image based from a packer file an ansible playbook.
#
# Required globals:
#   KEY_FILE
#   PROJECT
#   DOCKER_IMAGE
#
# Option globals
#   DOCKER_TAG: Default: latest
#   ZONE: Options: https://cloud.google.com/compute/docs/regions-zones#available Default: us-central1-a
#   ENVIRONMENT: Default: ''
#   MACHINE_TYPE: Options: https://cloud.google.com/compute/docs/machine-types  Default: f1-micro
#   SUBNET: Default: default
#   NETWORK_TIER: Default: PREMIUM
#   SCOPES
#   OS_IMAGE: https://cloud.google.com/container-optimized-os/docs/release-notes
#   GOOGLE_LOGGING_ENABLED

# Sample to run locally


source "$(dirname "$0")/common.sh"
enable_debug

# mandatory parameters

export

KEY_FILE=${KEY_FILE:?'KEY_FILE variable missing.'}
PROJECT=${PROJECT:?'PROJECT variable missing.'}
PACKER_FILE_PATH=${PACKER_FILE_PATH:?'Packer file missing'}
USER=${USER:?'USER variable missing.'}
CURRENT_DATE=`date +%Y%m%d-%H%M%S`
INSTANCE_NAME=${INSTANCE_NAME:-"ote-${CURRENT_DATE}"}

# optionals parameters
ZONE=${ZONE:-'us-central1-a'}
MACHINE_TYPE=${MACHINE_TYPE:-'f1-micro'}
SUBNET=${SUBNET:-'default'}
BUILD=${BUILD:-'true'}

info "Setting up environment".

echo "${KEY_FILE}" | base64 -d > /app/key-file.json
run gcloud auth activate-service-account --key-file /app/key-file.json --quiet
run gcloud config set project $PROJECT --quiet

info "Creating images"

if [[ ${BUILD} == true ]]; then
  info "Running packer"
  cp $PACKER_FILE_PATH template.tpl
  cp $PLAYBOOK_FILE_PATH playbook.tpl
  replace_tokens.sh
  
  if [[ ${DEBUG} == true ]]; then
    info "PACKER FILE: "
    run cat image-${BITBUCKET_BUILD_NUMBER}.json
    info "PLAYBOOK FILE: "
    run cat playbook-r4md0m.yml
  fi

  run packer build image-${BITBUCKET_BUILD_NUMBER}.json
fi

if [ "${status}" -eq 0 ]; then
  success "Execution successful."
else
  fail "Execution failed."
fi