#!/usr/bin/env bash

cat playbook.tpl | egrep '\$\{.*\}' | awk '{ print $2 }' | envsubst < playbook.tpl > playbook-r4md0m.yml
export PLAYBOOK_FILE_PATH=playbook-r4md0m.yml
cat template.tpl | egrep '\$\{.*\}' | awk '{ print $2 }' | envsubst < template.tpl > image-${BITBUCKET_BUILD_NUMBER}.json